# EE-463-Simulation-Project
Simulation project repository for EE463 Static Power Conversion I course.

## Question 1
Finished.
## Question 2 
### Products
#### Diodes
Digi-key Part number : [497-15246-5-ND](https://www.digikey.com/product-detail/en/smc-diode-solutions/12TQ200/1655-1003-ND/6022093)
#### Capacitors
Digi-key Part number : [1189-3928-ND](https://www.digikey.com/product-detail/en/rubycon/400USG680MEFC35X40/400USG680MEFC35X40-ND/6184452)

We used 4 12TQ200/S schottky diode for rectifier
We used 3 680uF aluminium electrolit capacitor to decrease ripple voltage to 47.2 V where mean voltage 300 V. 
## Question 3
Finished except Power Factor
